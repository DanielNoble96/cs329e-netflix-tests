#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, make_prediction
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "10040:\n3.0\n3.1\n3.8\n0.87\n")

    def test_eval_2(self):
        r = StringIO("7575:\n1592062\n178214\n4949:\n32679\n1180926\n577721\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "7575:\n4.1\n2.4\n4949:\n3.6\n3.5\n4.5\n0.94\n")

    def test_eval_3(self):
        r = StringIO("3427:\n2553714\n614785\n379936\n537527\n2332580\n375269\n445767\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "3427:\n4.4\n1.9\n3.8\n3.2\n4.0\n4.7\n3.5\n0.77\n")


    def test_pred_1(self):
        a = make_prediction(995, 714576)
        self.assertEqual(a, 1.1)

    def test_pred_2(self):
        a = make_prediction(10022, 268014)
        self.assertEqual(a, 3.3)

    def test_pred_3(self):
        a = make_prediction(10020, 1252652)
        self.assertEqual(a, 5.0)









# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
