#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, predict
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.3\n2.8\n3.4\n0.70\n")

    def test_eval_2(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n2488120\n317050\n1904905\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "1:\n3.7\n3.5\n3.8\n4.7\n3.8\n4.0\n0.64\n")

    def test_eval_3(self):
        r = StringIO("10013:\n2503691\n1222878\n843201\n2271154\n1346139\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10013:\n3.6\n3.2\n3.5\n3.7\n3.4\n0.81\n")

    def test_read_1(self):
        r = StringIO("10040:\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n")

    def test_key_error_1(self):
        r = StringIO("11111100:\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(w.getvalue(), "11111100:\n")

    def test_predict_1(self):
        self.assertEqual(predict(3,2,3,4),2.1)

    def test_predict_2(self):
        self.assertEqual(predict(3,2.5,3.5,4),2.6)



# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
