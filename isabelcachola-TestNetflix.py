#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, read_reader, make_predictions
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract
import re

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ------------
    # read_reader
    # ------------

    def test_read_reader1(self):
        r = StringIO("12870:\n739334\n315404\n1609741\n1509388\n")
        w = read_reader(r)
        self.assertEqual(w, [[12870, [739334, 315404, 1609741, 1509388]]])

    def test_read_reader2(self):
        r = StringIO("2699:\n65536\n81921\n1263617\n51203\n366596\n2320389\n1595396\n1714205\n2301984\n448549\n274469\n")
        w = read_reader(r)
        self.assertEqual(w, [[2699, [65536, 81921, 1263617, 51203, 366596, 2320389, 1595396, 1714205, 2301984, 448549, 274469]]])

    def test_read_reader3(self):
        r = StringIO("15425:\n270337\n581635\n2291721\n737294\n2439188\n1093655\n")
        w = read_reader(r)
        self.assertEqual(w, [[15425, [270337, 581635, 2291721, 737294, 2439188, 1093655]]])

    # -----------------
    # make_predictions
    # -----------------

    def test_make_predictions1(self):
        pred = make_predictions(752, 1728002)
        self.assertEqual(pred, 3.6)

    def test_make_predictions2(self):
        pred = make_predictions(8027, 2637760)
        self.assertEqual(pred, 3.4)

    def test_make_predictions3(self):
        pred = make_predictions(3333, 2465793)
        self.assertEqual(pred, 3.6)

    # -----
    # eval
    # -----

    def test_eval_check_input_format(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n6437:\n1808384\n585730\n1589251\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertTrue(re.match(r'(\d+:\n(\d+\n?)+)+', r.getvalue()))

    def test_eval_check_output_format(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n6437:\n1808384\n585730\n1589251\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertTrue(re.match(r'(\d+:\n(\d+\.\d+\n?)+)+', w.getvalue()))

    def test_eval_rsme_less_than_one(self):
        f = open('RunNetflix.in', 'r')
        r = StringIO(f.read())
        f.close()
        w = StringIO()
        netflix_eval(r, w)
        rsme = float(w.getvalue().split()[-1])
        self.assertLess(rsme, 1.0)
# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
