#!/usr/bin/env python3

from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract


class TestNetflix (TestCase):
    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n2.4\n2.4\n2.4\n0.90\n")

    def test_eval_2(self):
        r = StringIO("10005:\n254775\n1892654\n469365\n793736\n926698")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "10005:\n3.6\n3.8\n3.4\n3.3\n3.5\n")

    def test_eval_3(self):
        r = StringIO("10006:\n109333\n1982605\n15434853\n1632583")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "10006:\n3.8\n3.5\n3.8\n3.2\n")

    def test_eval_4(self):
        r = StringIO("10003:\n1515111")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "10003:\n2.9\n")


if __name__ == '__main__':
    main()


""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
