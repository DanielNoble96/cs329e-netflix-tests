#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import *
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # netflix_eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.76\n3.32\n4.25\n0.78\n")

    def test_noMovieAtStart(self):
        r = StringIO("2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "")
        
    def test_noCustomers(self):
        r = StringIO("10040:\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n")


    # ----
    # getRangeOfMovieRatingYears
    # ----

    # Validate that the range is what we know it is (only other way to test is to create custom cache)
    def test_validateRangeOfMovieRatingYears(self):
        x = getRangeOfMovieRatingYears()
        self.assertEqual(x, {'earliest': 1999, 'latest': 2005})

    # ----
    # getMoviesCustomersHaveRated
    # ----
    
    # Line of people but no one has rated any movies
    def test_NumMoviesSeenWithNoMovies(self):
        r = StringIO("2417853\n1207062\n2487973\n30878\n2647871\n1283744\n2488120\n317050\n1904905\n1989766\n14756\n1027056\n1149588\n1394012\n1406595\n2529547\n1682104\n2625019\n2603381\n1774623\n470861")
        x = getMoviesCustomersHaveRated( r )
        # X will be a dict of keys with no movies
        self.assertEqual(x, {})

    # Line of Movies with no one rating them
    def test_NumMoviesSeenWithNoPeople(self):
        r = StringIO("10040:\n1:\n")
        x = getMoviesCustomersHaveRated( r )
        self.assertEqual(x, {})

    # People with movies
    def test_NumMoviesSeenNormalCase(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n1:\n30878\n2647871\n1283744\n2488120\n317050\n1904905\n1989766\n14756\n1027056\n1149588\n1394012\n1406595\n2529547\n1682104\n2625019\n2603381\n1774623\n470861")
        x = getMoviesCustomersHaveRated( r )
        self.assertEqual(x[2417853], [10040])

    # People with movies
    def test_NumMoviesSeenNormalCase2(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n1:\n2417853\n30878\n2647871\n1283744\n2488120\n317050\n1904905\n1989766\n14756\n1027056\n1149588\n1394012\n1406595\n2529547\n1682104\n2625019\n2603381\n1774623\n470861")
        x = getMoviesCustomersHaveRated( r )
        self.assertEqual(x[2417853], [10040, 1])

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
